const Orders = require('../models/orders');


// @desc    Get orders
// @rote    GET /api/orders
// @access  Public


const getOrders = async (req, res) => {
    try {
        const order = await Orders.find();

        return res.status(200).json({
            success: true,
            count: order.length,
            data: order
        })
    } catch (e) {
        return res.status(500).json({
            success: false,
            error: "server error"
        })
    }
};

module.exports = getOrders;


