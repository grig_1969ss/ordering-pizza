require('dotenv').config();

const User = require('../models/user');
const express = require('express');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const app = express();

app.use(express.json());


exports.login = async (req, res) => {
    if (!req.body) return res.sendStatus(400);
    const {email, password} = req.body;

    const accessToken = jwt.sign({
        email: req.body.email,
    }, process.env.ACCESS_TOKEN_SECRET);



    const response = await User.findOne({email});
    if (!response) {
        return res.status(400).json({msg: "User not found"})
    }

    const isMuch = await bcrypt.compare(password, response.password);

    if (!isMuch) {
        return res.status(400).json({msg: "Incorrect password, try again"})
    }


    if (response) {
        res.send({data: response, accessToken: accessToken});
    } else {
        res.sendStatus(400)
    }
};