const User   = require('../models/user');
const bcrypt = require('bcryptjs');


// @desc    Add user
// @rote    POST /api/users
// @access  Public


exports.addUser = async (req, res) => {

    try {
        const {name, email, password, address} = req.body;
        const candidate         = await User.findOne({email});

        if (candidate) {
            return res.status(400).json({msg: "User such already"})
        }

        const hashedPassword = await bcrypt.hash(password, 12);
        const user           = new User({name, address, email, password: hashedPassword});
        await user.save();
        res.status(200).json({msg: "Thank you for registration"})


    } catch (e) {
        res.status(500).json({msg: "Something went those so, try again"})
    }

};


