const Orders = require('../models/orders');
const Pizza = require('../models/pizza');

// @desc    order
// @rote    POST /api/order
// @access  Public


const order = async (req, res) => {
    const {pizza, userId, amount} = req.body;
    const pizz = await Pizza.find({_id: pizza.map(item => item.id)});


    const order = new Orders({
        soldPizza: pizz,
        userId,
        amount,
        quantity: pizza.map(item => item.quantity)
    });


    await order.save();
    res.status(200).json({msg: "Bon Appétit"})

};

module.exports = order;


