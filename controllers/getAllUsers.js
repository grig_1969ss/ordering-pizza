const User = require('../models/user');

// @desc    Get all users
// @rote    GET /api/v1/users
// @access  Public

exports.getAllUsers = async (req, res) => {
    try {
        const user = await User.find();

        return res.status(200).json({
            success: true,
            count: user.length,
            data: user
        })
    } catch (e) {
        return res.status(500).json({
            success: false,
            error: "server error"
        })
    }
};