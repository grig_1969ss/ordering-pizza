const Pizza = require('../models/pizza');

// @desc    Get all pizzas
// @rote    GET /api/pizzas
// @access  Public

exports.getAllPizzas = async (req, res) => {

    try {
        const pizza = await Pizza.find();
        return res.status(200).json({
            success: true,
            count: pizza.length,
            data: pizza
        })
    } catch (e) {
        return res.status(500).json({
            success: false,
            error: "server error"
        })
    }
};