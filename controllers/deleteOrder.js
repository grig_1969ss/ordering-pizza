const Orders = require('../models/orders');

// @desc    Delete Order
// @rote    Delete /api/deleteorder
// @access  Public

const deleteOrder = async (req, res) => {
    try {
        console.log(req.params.id)
        const order = await Orders.findById([req.params.id]);
        console.log(order)
    
    } catch (e) {
        return res.status(500).json({
            success: false,
            error: "server error"
        })
    }
};

module.exports = deleteOrder;