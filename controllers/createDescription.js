const Description  = require('../models/description');


// @desc    Add description
// @rote    POST /api/description
// @access  Public


const addDescription = async (req, res) => {
    const {pizzaId, userId, description} = req.body;

    const sentDescription = new Description({
        pizzaId,
        userId,
        description
    });

    await sentDescription.save();
    res.status(200).json({msg: "Your description added"})

};

module.exports = addDescription;


