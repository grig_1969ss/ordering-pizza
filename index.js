const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const routes = require('./routes/routes');
const dotenv = require('dotenv');
const connectDB = require('./config/db');
const path = require('path');

const order = require('./controllers/order');
const getOrders = require('./controllers/getOrders');
const createDescription = require('./controllers/createDescription');


dotenv.config({path: './config/config.env'});


connectDB();



const app = express();



app.use(express.json());

if (process.env.NODE_ENV === 'production') {

    app.use(express.static("client/build"));

    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, '../client', 'build', 'index.html'))
    })
}

app.use(express.json());


app.use(morgan("dev"));


const PORT = process.env.PORT || 5000;

app.use(bodyParser.json());

app.use('/api/users', routes);
app.use('/api/login', routes);
app.use('/api/users/pizzas', routes);
app.post('/api/order', order);
app.get('/api/orders', getOrders);
app.post('/api/description', createDescription);

app.listen(PORT, console.log(`Server running in mode on ${PORT}`));
