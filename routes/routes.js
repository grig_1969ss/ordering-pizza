const express = require('express');
const router = express.Router();
const {addUser} = require('../controllers/createUser');
const {getUser} = require('../controllers/getUser');
const {login} = require('../controllers/login');
const {getAllUsers} = require('../controllers/getAllUsers');
const {getAllPizzas} = require('../controllers/getAllPizzas');


router
    .route('/')
    .get(getAllUsers)
    .post(addUser);

router
    .route('/login')
    .post(login);

router
    .route('/:id')
    .post(getUser);

router
    .route('/pizzas')
    .get(getAllPizzas);

module.exports = router;