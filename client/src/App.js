import React, {useState, useEffect} from 'react';
import {
    BrowserRouter as Router
} from "react-router-dom";
import {MainContext} from './context/MainContext';
import {useRoutes} from "./routes";
import {useAuth} from "./hooks/auth.hook";
import {NavBar} from "./pages/navBar/NavBar";
import 'materialize-css';

function App() {
    const res = [];
    const [ordered, setOrdered] = useState([]);
    const [count, setCount] = useState(0);
    const [historyCount, setHistoryCount] = useState('');
    const {token, logIn, logOut, userId} = useAuth();
    const isAuth = !!token;
    const routes = useRoutes(isAuth);
    const acc = ordered !== [] && ordered.map(item => item.quantity);
    const [quantitySum, setQuantitySum] = useState(0);


    useEffect(() => {
        if (count !== 0) {
            if (!isAuth) {
                localStorage.setItem("pizza", JSON.stringify({ordered}));
            } else {
                localStorage.setItem("logedUserPizza", JSON.stringify({ordered}))
            }
        }
        // eslint-disable-next-line
    }, [ordered]);

    useEffect(() => {
        if (acc.length > 0) {
            setQuantitySum(acc.reduce((accr, add) => accr + add));
        } else {
            setQuantitySum(0)
        }
    }, [acc]);

    const removeFromCart = async (e, id, buffState) => {
        e.preventDefault();

        const data = JSON.parse(localStorage.getItem('pizza'));
        if (data) {
            setHistoryCount(data.sentData)
        }
        // eslint-disable-next-line
        await buffState.find(pizza => {
            if (pizza._id === id) {
                const pizzas = [...ordered];
                if (pizzas.find((item) => item.id === id)) {
                    // eslint-disable-next-line
                    pizzas.map(el => {
                        if (el.id === id) {
                            if (el.quantity > 1) {
                                el.quantity -= 1;
                                setOrdered(pizzas);
                            }
                        }
                    })
                }
            }

        });

        setCount(count + 1);

    };

    const deletePizza = async (e, id, buffState) => {
        e.preventDefault();

        const data = JSON.parse(localStorage.getItem('pizza'));
        if (data) {
            setHistoryCount(data.sentData)
        }
        // eslint-disable-next-line
        await buffState.find(pizza => {
            
            if (pizza._id === id) {
                const pizzas = [...ordered];
                if (pizzas.find((item) => item.id === id)) {
                    if (ordered.length > 1) {
                        setOrdered(pizzas.filter(item => item.id !== id))
                    } else {
                        localStorage.removeItem('logedUserPizza');
                        setOrdered([]);
                    }

                }

            }
        });
        setCount(count - 1);
    };

    const addToCart = async (e, id, buffState) => {

        e.preventDefault();

        const data = JSON.parse(localStorage.getItem('pizza'));
        if (data) {
            setHistoryCount(data.sentData)
        }
        // eslint-disable-next-line
        await buffState.find(pizza => {
            if (pizza._id === id) {
                const pizzas = [...ordered];
                if (pizzas.find((item) => item.id === id)) {
                    // eslint-disable-next-line
                    pizzas.map(el => {
                        if (el.id === id) {
                            el.quantity += 1;
                            setOrdered(pizzas);
                        }
                    })
                } else {
                    res.push({id: pizza._id, price: pizza.price, quantity: 1});
                }
            }
        });
        if (res !== []) {
            setOrdered(prev => [...prev, ...res]);
        }

        setCount(count + 1);
    };


    if (!isAuth) {
        window.onload = () => {
            const orders = JSON.parse(localStorage.getItem("pizza"));
            if (orders) {
                setOrdered(Object.values(orders)[0]);

            }
        }
    } else {
        window.onload = () => {
            const orders = JSON.parse(localStorage.getItem("logedUserPizza"));
            if (orders) {
                setOrdered(Object.values(orders)[0]);

            }
        }
    }


    return (
        <MainContext.Provider
            value={{
                token,
                logIn,
                logOut,
                userId,
                isAuth,
                addToCart,
                ordered,
                historyCount,
                count,
                setOrdered,
                quantitySum,
                removeFromCart,
                deletePizza
            }}>
            <Router>
                <NavBar/>
                <div className={'container app'}>
                    {routes}
                </div>
            </Router>
        </MainContext.Provider>
    );
}

export default App;
