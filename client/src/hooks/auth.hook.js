import {useState, useCallback, useEffect} from 'react';

const storageName = 'user-data';

export const useAuth = () => {
    const [token, setToken] = useState(null);
    const [userId, setUserId] = useState(null);

    const logIn = useCallback((jwt, id) => {
        setToken(jwt);
        setUserId(id);
        localStorage.setItem(storageName, JSON.stringify({userId: id, token: jwt}));
        localStorage.removeItem("pizza");
    }, []);


    const logOut = useCallback(() => {
        setToken(null);
        setUserId(null);
        localStorage.removeItem(storageName);
        localStorage.removeItem("pizza");
    }, []);

    useEffect(() => {
        const data = JSON.parse(localStorage.getItem(storageName));
        if (data && data.token) {
            logIn(data.token, data.userId)
        }
    }, [logIn]);

    return {logIn, logOut, token, userId}
};