import {createContext} from 'react';

function noop() {
}


export const MainContext = createContext({
    token: null,
    userId: null,
    logIn: noop,
    logOut: noop,
    isAuth: false,
    ordered: [],
    addToCart: noop,
    removeFromCart: noop,
    deletePizza: noop
});

