import React, {useEffect, useState} from "react";
import {Link} from "react-router-dom";
import {useHttp} from "../../hooks/http.hook";
import {useMessage} from "../../hooks/message.hook";
import {Redirect} from "react-router-dom";
import './register.css';

export const Register = () => {
    const message = useMessage();
    const {loading, error, request, clearError} = useHttp();

    const [toLog, setSuccess] = useState({success: false});
    const [form, setForm] = useState({
        name: '',
        email: '',
        password: '',
        address: ''
    });

    useEffect(() => {
        message(error);
        clearError()
    },[message, error, clearError]);


    const handleChange = event => {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        })
    };


    const registerHandler = async e => {
        e.preventDefault();
        try {
            const data = await request('/api/users', "POST", {...form});
            setSuccess({success: true});
            message(data.msg)
        } catch (e) {

        }
    };

    return (
        <div className={'row'}>
            <div className="col s6 offset-s3">
                <div className="card green darken-1 reg-form">
                    <div className="card-content white-text">
                        <span className="card-title">Register</span>
                        <div>
                            <form onSubmit={registerHandler}>
                                <div className="input-field">
                                    <input
                                        id="name"
                                        type="text"
                                        className="validate inputAuth"
                                        name="name"
                                        onChange={handleChange}
                                        required
                                    />
                                    <label htmlFor="name">Name</label>
                                </div>
                                <div className="input-field">
                                    <input
                                        id="email"
                                        type="email"
                                        className="validate inputAuth"
                                        name="email"
                                        onChange={handleChange}
                                        required
                                    />
                                    <label htmlFor="email">Email</label>
                                </div>
                                <div className="input-field">
                                    <input
                                        id="password"
                                        type="password"
                                        className="validate inputAuth"
                                        name="password"
                                        onChange={handleChange}
                                        required
                                    />
                                    <label htmlFor="email">Password</label>
                                </div>
                                <div className="input-field">
                                    <input
                                        id="address"
                                        type="text"
                                        className="validate inputAuth"
                                        name="address"
                                        onChange={handleChange}
                                        required
                                    />
                                    <label htmlFor="address">Address</label>
                                </div>
                                <div className="card-action">
                                    <button
                                        type={'submit'}
                                        className={'btn yellow darken-4'}
                                        disabled={loading}
                                        style={{marginRight: 15}}>Register
                                    </button>
                                    {toLog.success && <Redirect to='/login'/>}
                                    <Link to={'/login'}><button type={'submit'} className={'btn darken-4'}
                                            style={{marginRight: 15}}>Login
                                    </button></Link>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
};