import React, {useContext, useState, useEffect} from "react";
import {useHttp} from "../../hooks/http.hook";
import {MainContext} from "../../context/MainContext";
import {useMessage} from "../../hooks/message.hook";
import './pizzaList.css'; 


export const PizzaList = () => {

    const auth = useContext(MainContext);
    const message = useMessage();
    const [pizzas, setPizzas] = useState([]);
    const [textValue, setTextValue] = useState(pizzas.map(item => item.description))
    const {request, loading} = useHttp();


    useEffect(() => {
        request('/api/users/pizzas', 'GET')
            .then(res => setPizzas(res.data))
    }, [request]);


    const handleChange = (e) => {
        setTextValue({
            ...textValue,
            [e.target.name]: e.target.value
        })
    };


    const handleClick = async (e, index, id) => {
        e.preventDefault();
        const sentData = {
            userId: JSON.parse(localStorage.getItem('user-data')).userId,
            pizzaId: id,
            description: textValue[index]
        };

        localStorage.setItem("pizza", JSON.stringify({sentData}));
        if(!textValue !== ''){
            const data = await request('/api/description', "POST", {...sentData});
            message(data.msg);
        } else{
            message("This input required")
        }


    };

    return (
        <div className={'main'}>
            {
                pizzas ? pizzas.map((pizza, index) => {
                    return (
                        <div key={pizza._id} className={'grid-item'}>
                            <div className="pizzaContainer">
                                <img src={`${pizza.img}`} alt="pizza" width={'300px'}/>
                                <p>{pizza.name}</p>
                                {
                                    !auth.isAuth ?
                                        <p>{pizza.description}</p> :
                                        <div className="rows">
                                            <label htmlFor="textarea" className='labelDescription'>Description</label>
                                            <form className="col">
                                                <div className="rows">
                                                    <div className="input-fields">
                                                    <textarea
                                                        id={"textarea"}
                                                        placeholder="Enter your preferences description"
                                                        key={index}
                                                        name={pizza.description}
                                                        className="materialize"
                                                        value={textValue[index]}
                                                        onChange={handleChange}>
                                                    </textarea>

                                                    </div>
                                                </div>

                                                <button
                                                    className="btn waves-light"
                                                    onClick={(e) => handleClick(e, pizza.description, pizza._id)}
                                                    name="action"
                                                    disabled={loading}
                                                >Add
                                                </button>
                                            </form>
                                        </div>
                                }

                            </div>
                            <p>{`Price ${pizza.price} $`}</p>
                            <a href={'/buy'} className="btn-floating btn-large  waves-light red"
                               onClick={e => auth.addToCart(e, pizza._id, pizzas)}><i
                                className="material-icons" id={'btn-add'}>add</i></a>
                        </div>
                    )
                }) : <h2>Sorry. We do not have pizza</h2>
            }
        </div>
    )
};