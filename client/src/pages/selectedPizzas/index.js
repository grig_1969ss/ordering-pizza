import React, {useContext, useEffect, useState} from "react";
import {MainContext} from "../../context/MainContext";
import {useHttp} from "../../hooks/http.hook";
import {useMessage} from "../../hooks/message.hook";
import {useHistory} from "react-router-dom";
import './selectedPizza.css';


export const SelectedPizzas = () => {
    const {
        ordered,
        setOrdered,
        addToCart,
        removeFromCart,
        deletePizza,
        quantitySum,
        isAuth,
    } = useContext(MainContext);
    const message = useMessage();
    const [pizza, setPizza] = useState([]);
    const [pizzas, setPizzas] = useState([]);
    const [allPrice, setAllPrice] = useState(0);
    const {request, loading} = useHttp();
    const history = useHistory();

    useEffect(() => {
        request('/api/users/pizzas', 'GET')
            .then(res => setPizza(res.data))
    }, [request]);

    useEffect(() => {
        const res = [];
        // eslint-disable-next-line
        pizza.map(pizza => {
        // eslint-disable-next-line
            ordered.map(item => {
                if (item.id === pizza._id) {
                    res.push(pizza)
                }
            })
        });
        setPizzas(res)
        // eslint-disable-next-line
    }, [pizza, quantitySum]);

    useEffect(() => {
        const res = ordered.map(item => item.price * item.quantity);
        if (res.length > 0) {
            setAllPrice(res.reduce((accr, add) => accr + add))
        } else {
            setAllPrice(0)
        }
        // eslint-disable-next-line
    }, [quantitySum]);

    const buyPizzas = async () => {
        if (isAuth) {
            localStorage.removeItem('logedUserPizza');
            setOrdered([]);
            if (ordered.length > 0) {
                const sendData = {
                    pizza: ordered,
                    userId: JSON.parse(localStorage.getItem('user-data')).userId,
                    amount: allPrice
                };

                const data = await request('/api/order', "POST", {...sendData});
                message(data.msg);

            } else {

                message("You are have not order")
            }
        } else {
            history.push('/register')
        }

    };


    return (
        <div>
            {
                quantitySum !== 0 ? <>
                    <div className={'select'}>
                        <h2>Your select pizzas</h2>
                    </div>
                    <div className="buttonWrapper">
                        <p className="allPrice">{`Total Price ${allPrice}$`}</p>
                        <button onClick={buyPizzas} className=" waves-light btn">Buy Now
                        </button>
                    </div>
                </> : <div className={'select'}>
                    <h2>You are not select pizza</h2>
                </div>
            }


            <div className={'main-selected'}>
                {
                    pizzas ? pizzas.map(pizza => {
                        return (
                            <div key={pizza._id} className={'grid-item'}>
                                <div className="pizzaContainer">
                                    <img src={`${pizza.img}`} width={'300px'} alt={'imgPizza'}/>
                                    <p>{pizza.name}</p>
                                    <p>{pizza.description}</p>
                                    <p>Quantity:{
                                        // eslint-disable-next-line
                                        ordered.map(order => {
                                            if (order.id === pizza._id) {
                                                return order.quantity && order.quantity
                                            }
                                        })}</p>
                                </div>
                                <p>{`Price ${pizza.price} $`}</p>
                                <a href={'/buy'} className="btn-floating  waves-light red"
                                   onClick={e => addToCart(e, pizza._id, pizzas)}>
                                    <i className="material-icons" id={'btn-add'}>add</i></a>
                                <a href={'/buy'} className="btn-floating waves-light red"
                                   onClick={(e) => removeFromCart(e, pizza._id, pizzas)}>
                                    <i className="material-icons" id={'btn-min'}>-</i></a>
                                <button
                                    className="btn  waves-light"
                                    onClick={(e) => deletePizza(e, pizza._id, pizzas)}
                                    name="action"
                                    disabled={loading}
                                >Delete
                                </button>
                            </div>
                        )
                    }) : <h2>Sorry. We do not have pizza</h2>
                }
            </div>
        </div>
    )
};