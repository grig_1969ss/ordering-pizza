import React from "react";
import { CarouselProvider, Slider, Slide } from 'pure-react-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import 'pure-react-carousel/dist/react-carousel.es.css';
import './home.css'



export class Home  extends React.Component {
    render() {
        return (
            <CarouselProvider
                naturalSlideWidth={100}
                naturalSlideHeight={125}
                totalSlides={3}
            >
                <Slider>

                    <Slide index={0}> <div>
                        <img src="https://images.alphacoders.com/959/thumb-1920-959369.jpg" alt={'pizza'}/>
                    </div></Slide>
                    <Slide index={1}><div>
                        <img src="https://images8.alphacoders.com/644/thumb-1920-644361.jpg" alt={'pizza'}/>
                    </div></Slide>
                    <Slide index={2}><div>
                        <img src="https://images4.alphacoders.com/276/276908.jpg" alt={'pizza'}/>
                    </div></Slide>
                </Slider>
            </CarouselProvider>
        );
    }
}