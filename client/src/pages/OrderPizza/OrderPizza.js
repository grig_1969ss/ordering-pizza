import React from "react";

import './order.css';
import {PizzaList} from "../pizzaList/PizzaList";

export const OrderPizza = () => {
    return (
        <div>
            <div className={'select'}>
                <h2>Select your pizza</h2>
            </div>
            <PizzaList/>
        </div>
    )
};