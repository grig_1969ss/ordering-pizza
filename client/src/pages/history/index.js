import React, {useEffect, useState} from "react";
import "./history.css";
import {useHttp} from "../../hooks/http.hook";


export const History = () => {

    const [soldPizza, setSoldPizza] = useState([]);
    const {request} = useHttp();
    const userId = JSON.parse(localStorage.getItem("user-data")).userId;
    const [pizzas, setPizzas] = useState([]);
    const pizza = [];


    /* eslint-disable-next-line react/jsx-no-target-blank */

    useEffect(() => {
        return setPizzas(pizza);
        // eslint-disable-next-line
    },[soldPizza]);

    useEffect(() => {
        request('/api/orders', "GET")
            .then(res => setSoldPizza(res.data))
    }, [request]);


    for (let i = 0; i < soldPizza.length; i++) {
        if (soldPizza[i].userId.join('') === userId) {
            pizza.push(...soldPizza[i].soldPizza)
        }
    }





    return (
        <div>
            <div>
                {
                    soldPizza.length <= 0 ? <h2 className={'head'}>You have no purchase pizza</h2> : <div>
                        <h2 className={'head'}>Your bought pizzas</h2>
                        <table>
                            <thead>
                            <tr>
                                <th>Image</th>
                                <th>Pizza Name</th>
                                <th>Pizza Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                pizzas.length > 0 && pizzas.map((pizza, index) => {
                                    return (
                                        <tr key={index}>
                                            <td><img src={`${pizza.img}`} alt="img pizza" width={'100px'}/></td>
                                            <td><p>{pizza.name}</p></td>
                                            <td><p>{pizza.price}$</p></td>
                                        </tr>
                                    )
                                })
                            }
                            </tbody>
                        </table>
                    </div>
                }

            </div>
        </div>
    )
};