import React, {useContext, useEffect} from "react";
import {Link, useHistory} from "react-router-dom";
import {MainContext} from "../../context/MainContext";
import './navBar.css';


export const NavBar = () => {
    const {logOut, isAuth, quantitySum} = useContext(MainContext);
    const history = useHistory();
    const logOutHandler = e => {
        e.preventDefault();
        logOut();
        history.push('/');
    };

    const hideOrShowMenu = () => {
        let top = document.getElementById("myTopnav");
        if (top.className === "topnav") {
            top.classList.add("responsive")
        } else {
            top.className = "topnav";
        }
    };

    return (
        <div className="topnav">
            <div className="topnav" id="myTopnav">
                {
                    isAuth ? <>
                        <Link to="/" className="brand-logo"><img
                            src="https://dewey.tailorbrands.com/production/brand_version_mockup_image/314/3524516314_69564182-6542-41d4-895a-8d287185311c.png?cb=1597395501"
                            alt="" width={'30px'}/></Link>
                        <Link to="/order" onClick={hideOrShowMenu}>Order Pizza</Link>
                        <Link to="/history" onClick={hideOrShowMenu}>History</Link>
                        <Link to="/cart" onClick={hideOrShowMenu}><i className="small material-icons">add_shopping_cart</i>{quantitySum}</Link>
                        <a href="/" onClick={logOutHandler}>Log Out</a>

                        <Link to="#" className="icon" onClick={hideOrShowMenu}>
                            <i className="material-icons">local_pizza</i>
                        </Link>
                    </> : <>
                        <Link
                            to="/"
                            className="brand-logo">
                            <img
                                src="https://dewey.tailorbrands.com/production/brand_version_mockup_image/314/3524516314_69564182-6542-41d4-895a-8d287185311c.png?cb=1597395501"
                                alt="" width={'30px'}/></Link>
                        <Link to="/order" className="i" onClick={hideOrShowMenu}>Order Pizza</Link>
                        <Link to="/login" className="i" onClick={hideOrShowMenu}>Login</Link>
                        <Link to="/register" className="i" onClick={hideOrShowMenu}>Register</Link>
                        <Link to="/cart" className="i" onClick={hideOrShowMenu}><i
                            className="small material-icons">add_shopping_cart</i>{quantitySum}</Link>
                        <Link to="#" className="icon" onClick={hideOrShowMenu}>
                            <i className="material-icons">local_pizza</i>
                        </Link>
                    </>
                }

            </div>
        </div>
    )
};