import React, {useContext, useEffect, useState} from "react";
import {useHttp} from "../../hooks/http.hook";
import {useMessage} from "../../hooks/message.hook";
import {MainContext} from "../../context/MainContext";
import './auth.css';

export const Auth = () => {
    const auth = useContext(MainContext);
    const message = useMessage();
    const pizzaFromLocal = JSON.parse(localStorage.getItem('pizza'));
    const {loading, clearError, error, request} = useHttp();

    const [form, setForm] = useState({
        email: '',
        password: ''
    });

    useEffect(() => {
        message(error);
        clearError()
    }, [message, error, clearError]);


    const handleChange = event => {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        })
    };


    const loginHandler = async e => {
        e.preventDefault();
        if(pizzaFromLocal){
            localStorage.removeItem('pizza')
        }
        try {
            const data = await request("/api/users/login", "POST", {...form});
            auth.logIn(data.accessToken, data.data._id)
        } catch (e) {

        }
    };


    return (
        <div className={'row'}>
            <div className="col s6 offset-s3">
                <div className="card green darken-1 log-form">
                    <div className="card-content white-text">
                        <div>
                            <span className="card-title">Login</span>
                            <form onSubmit={loginHandler}>
                                <div className="input-field">
                                    <input
                                        id="email"
                                        type="email"
                                        className="validate inputAuth"
                                        name="email"
                                        onChange={handleChange}
                                        required
                                    />
                                    <label htmlFor="email">Email</label>
                                </div>
                                <div className="input-field">
                                    <input
                                        id="password"
                                        type="password"
                                        className="validate inputAuth"
                                        name="password"
                                        onChange={handleChange}
                                        required
                                    />
                                    <label htmlFor="email">Password</label>
                                </div>
                                <div className="card-action">
                                    <button
                                        type={'submit'}
                                        className={'btn yellow darken-4'}
                                        disabled={loading}
                                        style={{marginRight: 15}}>Login
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
};