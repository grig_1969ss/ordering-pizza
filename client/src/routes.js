import React from "react";
import {
    Switch,
    Route,
    Redirect

} from "react-router-dom";
import {Home} from './pages/home/Home';
import {Auth} from './pages/auth/Auth';
import {OrderPizza} from './pages/OrderPizza/OrderPizza';
import {Register} from "./pages/register/Register";
import {History} from "./pages/history";
import {SelectedPizzas} from "./pages/selectedPizzas";



export const useRoutes = isAuth => {

  if(isAuth){
      return(
          <Switch>
              <Route exact path={'/'}>
                  <Home/>
              </Route>
              <Route exact path={'/history'}>
                  <History/>
              </Route>
              <Route exact path={'/cart'}>
                  <SelectedPizzas/>
              </Route>
              <Route exact path={'/order'}>
                  <OrderPizza/>
              </Route>
              <Redirect to={'/order'}/>
          </Switch>
      )
  }
  return (
      <Switch>
          <Route exact path={'/'}>
              <Home/>
          </Route>
          <Route exact path={'/register'}>
              <Register/>
          </Route>
          <Route exact path={'/login'}>
              <Auth/>
          </Route>
          <Route exact path={'/order'}>
              <OrderPizza/>
          </Route>
          <Route exact path={'/cart'}>
                  <SelectedPizzas/>
          </Route>
          <Redirect to={'/'}/>
      </Switch>
  )
};