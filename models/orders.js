const mongoose = require('mongoose');

const Orders = new mongoose.Schema({
    userId: [{
        type: mongoose.Types.ObjectId, ref: 'user'
    }],
    soldPizza: {
        type: Object
    },
    amount: {
        type: Number,
    },
    quantity:{
        type: Array
    },
    createAt: {
        type: Date,
        default: Date.now
    },

});

module.exports = mongoose.model('orders', Orders);