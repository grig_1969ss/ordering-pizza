const mongoose = require('mongoose');

const Description = new mongoose.Schema({
    userId: [{type: mongoose.Types.ObjectId, ref: 'user'}],
    pizzaId: [{type: mongoose.Types.ObjectId, ref: 'pizza'}],
    description: {
        type: String
    },
    createAt: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('descriptions', Description);