const mongoose = require('mongoose');

const User = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: [true, 'Please type your name...']
    },
    email: {
        type: String,
        required: [true, 'Please type your email...']
    },
    password: {
        type: String,
        required: [true, 'Please type your password...']
    },
    address: {
        type: String,
        required: [true, 'Please type your address...']
    },
    createAt: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('user', User);