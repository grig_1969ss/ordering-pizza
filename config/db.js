const mongoose = require('mongoose');
const config = require('./key');

const connectDB = async () => {

    try {
       await mongoose.connect(config.mongoURI, {
            useNewUrlParser:    true,
            useCreateIndex:     true,
            useUnifiedTopology: true
        });

        console.log(`MongoDB connected . . .`)
    } catch (e) {
        console.log(`Error: ${e.message}`);
        process.exit(1)
    }
};

module.exports = connectDB;